using System;
using Android.OS;

namespace TagWaldo
{
	public class ServiceConnectedEventArgs : EventArgs
	{
		public IBinder Binder { get; set; }
	}
}