﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Util;
using Android.Locations;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TagWaldo
{
	[Activity (Label = "AndroidLocationService", MainLauncher = true)]
	public class MainActivity : Activity
	{
		private static string API_KEY = "AIzaSyAk6Z_6jVm1tPpjLPmz-91fdWnMd-v4yTI";
		private static string URL_BASE="https://maps.googleapis.com/maps/api/geocode/json?latlng={0},{1}&key={2}";
		readonly string logTag = "MainActivity";
		bool gpsOn = false;
		bool restart = false;
		GoogleAddress googleAddress = new GoogleAddress();
		// make our labels
		TextView latText;
		TextView longText;
		Button gpsCycle, closeApp;

		#region Lifecycle

		//Lifecycle stages
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.Main);

			//getters
			latText = FindViewById<TextView> (Resource.Id.lat);
			longText = FindViewById<TextView> (Resource.Id.lng);
			gpsCycle = FindViewById<Button> (Resource.Id.myButton);
			closeApp = FindViewById<Button> (Resource.Id.closeButton);

			gpsCycle.Click += delegate(object s, EventArgs eve) {

				if(!gpsOn){
					if(restart)
						App.Current.LocationService.StartLocationUpdates();
				// This event fires when the ServiceConnection lets the client (our App class) know that
				// the Service is connected. We use this event to start updating the UI with location
				// updates from the Service
				App.Current.LocationServiceConnected += (object sender, ServiceConnectedEventArgs e) => {
//						App.Current.LocationService.StartLocationUpdates();
					Log.Debug (logTag, "ServiceConnected Event Raised");
					// notifies us of location changes from the system
					App.Current.LocationService.LocationChanged += HandleLocationChanged;
					//notifies us of user changes to the location provider (ie the user disables or enables GPS)
					App.Current.LocationService.ProviderDisabled += HandleProviderDisabled;
					App.Current.LocationService.ProviderEnabled += HandleProviderEnabled;
					// notifies us of the changing status of a provider (ie GPS no longer available)
					App.Current.LocationService.StatusChanged += HandleStatusChanged;


				};
					gpsOn=true;
					gpsCycle.Text="Stop location service";
				}
				else{
					gpsOn = false;
					restart = true;
					gpsCycle.Text="Start location service";
					App.Current.LocationService.StopLocationUpdates();
					latText.Text = "stopped";
					longText.Text = "stopped";
					}
				};
			closeApp.Click += delegate {
				MoveTaskToBack(true);
			};
		}

		protected override void OnPause()
		{
			Log.Debug (logTag, "Location app is moving to background");
			base.OnPause ();
		}

		protected override void OnResume()
		{
			Log.Debug (logTag, "Location app is moving into foreground");
			base.OnResume();
		}

		protected override void OnDestroy ()
		{
			Log.Debug (logTag, "Location app is becoming inactive");
			App.Current.LocationService.StopLocationUpdates ();
			base.OnDestroy ();
		}

		#endregion

		#region Android Location Service methods

		///<summary>
		/// Updates UI with location data
		/// </summary>
		public void HandleLocationChanged(object sender, LocationChangedEventArgs e)
		{
			//print time
			var time = DateTime.Now.TimeOfDay;
			Console.WriteLine ("Current time is {0}", time.ToString());


			Android.Locations.Location location = e.Location;
			Log.Debug (logTag, "Foreground updating");

			// these events are on a background thread, need to update on the UI thread
			RunOnUiThread (() => {
				latText.Text = String.Format ("Latitude: {0}", Math.Round(location.Latitude,4));
				longText.Text = String.Format ("Longitude: {0}", Math.Round(location.Longitude,4));
			});

			var url = String.Format(URL_BASE,location.Latitude, location.Longitude, API_KEY);

			Task<string> addressJSON = null;
			Task.Factory.StartNew (delegate {
				addressJSON = googleAddress.DownloadFile (url);
			}).ContinueWith (task => {
				var temp1 = JObject.Parse(addressJSON.Result);
				var temp1String = (string)temp1 ["results"].ToString().Trim();

				JArray temp2 = (JArray) JsonConvert.DeserializeObject(temp1String);
				var json2 = temp2 [0].ToString ();

				var temp3 = JObject.Parse (json2);
				var addressString = (string)temp3 ["formatted_address"].ToString ();
				Console.WriteLine( addressString);
			}, TaskScheduler.FromCurrentSynchronizationContext());


		}

		public void HandleProviderDisabled(object sender, ProviderDisabledEventArgs e)
		{
			Log.Debug (logTag, "Location provider disabled event raised");
		}

		public void HandleProviderEnabled(object sender, ProviderEnabledEventArgs e)
		{
			Log.Debug (logTag, "Location provider enabled event raised");
		}

		public void HandleStatusChanged(object sender, StatusChangedEventArgs e)
		{
			Log.Debug (logTag, "Location status changed, event raised");
		}

		#endregion

	}
}


