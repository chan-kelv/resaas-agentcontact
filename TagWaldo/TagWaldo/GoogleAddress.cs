﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using ModernHttpClient;

namespace TagWaldo
{
	public class GoogleAddress
	{
		public async Task<string> DownloadFile(string url){
			try{
				var httpClient = new HttpClient(new NativeMessageHandler());
				var address =  await httpClient.GetStringAsync(url);
				return address;
			}
			catch(OperationCanceledException){
				Console.WriteLine ("error downloading file");
				return null;
			}
		}

	}
}

