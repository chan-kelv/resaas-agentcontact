﻿using System;
using System.Drawing;

using Foundation;
using UIKit;
using CoreLocation;
using System.Threading.Tasks;

namespace TagWaldo_iOS
{
	public partial class TagWaldo_iOSViewController : UIViewController
	{

		public static LocationManager Manager { get; set;}


		public TagWaldo_iOSViewController (IntPtr handle) : base (handle)
		{
			// As soon as the app is done launching, begin generating location updates in the location manager
			Manager = new LocationManager(); //turning on gps depends on switch condition
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		#region View lifecycle

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

//			startUpdate ();

				UIApplication.Notifications.ObserveDidEnterBackground ((sender, args) => {
					Console.WriteLine("Enter Background");
					Manager.LocationUpdated -= HandleLocationChanged; //stops updating lat/lng on screen
				});

				UIApplication.Notifications.ObserveDidBecomeActive ((sender, args) => {
					Console.WriteLine("Become Active");
					Manager.LocationUpdated += HandleLocationChanged; //starts updating lat/lng on screen
				});
			
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
		}
		#endregion


		#region Public Methods
		public void HandleLocationChanged (object sender, LocationUpdatedEventArgs e)
		{
			// Handle foreground updates
			CLLocation location = e.Location;

			latField.Text = location.Coordinate.Longitude.ToString ();
			lngField.Text = location.Coordinate.Latitude.ToString ();

			Console.WriteLine ("foreground updated");
		}


		partial void UISwitch14_ValueChanged (UISwitch sender)
		{
			if(OnOff.On)
				Manager.StartLocationUpdates();
			else
				Manager.StopLocationUpdates();
		}
		#endregion
	}
}

