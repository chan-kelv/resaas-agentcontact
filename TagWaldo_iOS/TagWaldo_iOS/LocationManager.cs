﻿using System;
using CoreLocation;
using UIKit;
using Foundation;
using System.Threading.Tasks;

namespace TagWaldo_iOS
{
	public class LocationManager
	{
		int HALT_TIME = 10;
		public string GEOCODE_KEY = "AIzaSyB5_Na3xtKoTq90TOmswI6oI9oN1lDA0NE";

		// event for the location changing
		public event EventHandler<LocationUpdatedEventArgs> LocationUpdated = delegate { };

		protected CLLocationManager locMgr;

		public LocationManager ()
		{
			this.locMgr = new CLLocationManager ();
			// iOS 8 has additional permissions requirements
			//
			if (UIDevice.CurrentDevice.CheckSystemVersion (8, 0)) {
				locMgr.RequestAlwaysAuthorization (); // works in background
				//locMgr.RequestWhenInUseAuthorization (); // only in foreground
			}

			LocationUpdated += PrintLocation;
		}

		public CLLocationManager LocMgr {
			get { return this.locMgr; }
		}

		public void StartLocationUpdates ()
		{
			if (CLLocationManager.LocationServicesEnabled) {
				//set the desired accuracy, in meters
				LocMgr.DesiredAccuracy = 1;
				LocMgr.StartUpdatingLocation ();
				LocMgr.LocationsUpdated += UpdateLocation;
			}
		}

		//This will keep going in the background and the foreground
		public void PrintLocation (object sender, LocationUpdatedEventArgs e)
		{
			CLLocation location = e.Location;
			Console.WriteLine ("Longitude: " + location.Coordinate.Longitude);
			Console.WriteLine ("Latitude: " + location.Coordinate.Latitude);

			var geolocUrl = String.Format ("https://maps.googleapis.com/maps/api/geocode/json?latlng={0},{1}&key={2}",
				                location.Coordinate.Latitude, location.Coordinate.Longitude, GEOCODE_KEY);
			new Address (geolocUrl);
		}

		public void UpdateLocation (object sender, CLLocationsUpdatedEventArgs e)
		{
			// fire our custom Location Updated event
			LocationUpdated (this, new LocationUpdatedEventArgs (e.Locations [e.Locations.Length - 1]));
			LocMgr.StopUpdatingLocation ();

			//delays update until next call
			//note that apple restricts the limit on how long a background task needs to return
			//background = 600s = 10min
			//the time limit can be modified using "extendBackgroundRunningTime"
			//http://developer.radiusnetworks.com/2014/11/13/extending-background-ranging-on-ios.html
			Console.WriteLine("Stop");
			NSTimer.CreateScheduledTimer(TimeSpan.FromSeconds (HALT_TIME), delegate {
				LocMgr.StartUpdatingLocation();
			});
		}

		public void StopLocationUpdates(){
			LocMgr.StopUpdatingLocation ();
			LocMgr.LocationsUpdated -= UpdateLocation;
		}
	}
}