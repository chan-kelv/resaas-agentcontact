﻿using System;
using System.Linq;
using System.Collections.Generic;

using Foundation;
using UIKit;
using System.IO;

namespace TagWaldo_iOS
{
	
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations

		public string GEOCODE_KEY = "AIzaSyB5_Na3xtKoTq90TOmswI6oI9oN1lDA0NE";
		public override UIWindow Window {
			get;
			set;
		}
		
		// This method is invoked when the application is about to move from active to inactive state.
		// OpenGL applications should use this method to pause.
		public override void OnResignActivation (UIApplication application)
		{
		}
		
		// This method should be used to release shared resources and it should store the application state.
		// If your application supports background exection this method is called instead of WillTerminate
		// when the user quits.
		public override void DidEnterBackground (UIApplication application)
		{
			Console.WriteLine ("App entering background state.");
		}
		
		// This method is called as part of the transiton from background to active state.
		public override void WillEnterForeground (UIApplication application)
		{
			Console.WriteLine ("App will enter foreground");
		}
		
		// This method is called when the application is about to terminate. Save data, if needed.
		public override void WillTerminate (UIApplication application)
		{
		}

		// This method is invoked when the application has loaded its UI and its ready to run
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			return true;
		}

		public void RenderStream (Stream stream)
		{
			var reader = new System.IO.StreamReader (stream);

//			InvokeOnMainThread (delegate {
//				var view = new UIViewController ();
//				view.View.BackgroundColor = UIColor.White;
//				var label = new UILabel (new CGRect (20, 60, 300, 80)){
//					Text = "The HTML returned by the server:"
//				};
//				var tv = new UITextView (new CGRect (20, 140, 300, 400)){
//					Text = reader.ReadToEnd();
				
//				};
//				view.Add (label);
//				view.Add (tv);
//
//				navigationController.PushViewController (view, true);
//			});		
			Console.WriteLine(reader.ReadToEnd ());
		}
	}
}

