//
// This file contains the sample code to use System.Net.WebRequest
// on the iPhone to communicate with HTTP and HTTPS servers
//
// Author:
//   Miguel de Icaza
//

using System;
using System.Net;
using Foundation;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Json;
using System.Collections.Generic;


namespace TagWaldo_iOS
{
	public class Address {
		
		public Address (string url)
		{
			HttpSample (url);
//			this.ad = ad;
		}
		
		//
		// Asynchronous HTTP request
		//
		public void HttpSample (string url)
		{

//			Application.Busy ();
			var request = WebRequest.Create (url);
			request.BeginGetResponse (FeedDownloaded, request);
		}
		
		//
		// Invoked when we get the stream back from the twitter feed
		// We parse the RSS feed and push the data into a 
		// table.
		//
		void FeedDownloaded (IAsyncResult result)
		{
//			Application.Done ();
			var request = result.AsyncState as HttpWebRequest;
			
			try {
				var response = request.EndGetResponse (result);
				RenderStream (response.GetResponseStream ());
			} catch (Exception e) {
				Debug.WriteLine (e);
			}
		}
		public void RenderStream (Stream stream)
		{
			var reader = new System.IO.StreamReader (stream);
//			Console.WriteLine(reader.ReadToEnd ());
			var jsonReturn = reader.ReadToEnd();
			extractStreet (jsonReturn);
		}

		public void extractStreet(string json){
			var temp1 = JObject.Parse (json);
			var temp1String = (string)temp1 ["results"].ToString().Trim();

			JArray temp2 = (JArray) JsonConvert.DeserializeObject(temp1String);
			var json2 = temp2 [0].ToString ();

			var temp3 = JObject.Parse (json2);
			var addressString = (string)temp3 ["formatted_address"].ToString ();
			Console.WriteLine( addressString);
		}


	}
}
