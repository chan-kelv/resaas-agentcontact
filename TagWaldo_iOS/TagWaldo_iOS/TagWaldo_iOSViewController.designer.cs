// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TagWaldo_iOS
{
	[Register ("TagWaldo_iOSViewController")]
	partial class TagWaldo_iOSViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel latField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lngField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UISwitch OnOff { get; set; }

		[Action ("UISwitch14_ValueChanged:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void UISwitch14_ValueChanged (UISwitch sender);

		void ReleaseDesignerOutlets ()
		{
			if (latField != null) {
				latField.Dispose ();
				latField = null;
			}
			if (lngField != null) {
				lngField.Dispose ();
				lngField = null;
			}
			if (OnOff != null) {
				OnOff.Dispose ();
				OnOff = null;
			}
		}
	}
}
