﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using ZXing.Mobile;

namespace AgentContact
{
	[Activity (Label = "AgentContact", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{

		MobileBarcodeScanner scanner;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			ActionBar.Hide ();
			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			#region qrScan
			//Create a new instance of our Scanner
			scanner = new MobileBarcodeScanner(this);

			//use the scanner
			var qrScanner = FindViewById<Button> (Resource.Id.qrScan);
			qrScanner.Click += async delegate {

				//Tell our scanner we want to use a custom overlay instead of the default
				scanner.UseCustomOverlay = true;

				//Inflate our custom overlay from a resource layout
				View customOverlay = LayoutInflater.FromContext(this).Inflate(Resource.Layout.customOverlay, null);

				//Find the button from our resource layout and wire up the click event
				var flash = customOverlay.FindViewById<Button>(Resource.Id.buttonZxingFlash);
				flash.Click += (sender, e) => scanner.ToggleTorch();

				//Set our custom overlay
				scanner.CustomOverlay = customOverlay;

				//Start scanning!
				var result = await scanner.Scan();

				HandleScanResult(result);
			};


			#endregion

			#region makeQR
			//make qr
			var myQr = FindViewById<Button> (Resource.Id.myID);
			myQr.Click += delegate {
				StartActivity(typeof(myQrActivity));
			};

			#endregion
		}

		void HandleScanResult (ZXing.Result result)
		{
			string msg = "";

			if (result != null && !string.IsNullOrEmpty(result.Text))
				msg = "Found Barcode: " + result.Text;
			else
				msg = "Scanning Canceled!";

			this.RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
		}

	}
}


