﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using ZXing;
using ZXing.Rendering;
using ZXing.Common;
using ZXing.QrCode.Internal;
using ZXing.QrCode;

namespace AgentContact
{
	[Activity (Label = "myQrActivity")]			
	public class myQrActivity : Activity
	{
		ImageView qrView;
		EditText userName;
		TextView desc;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.myQr);
			ActionBar.Hide ();

//			string qrText = "www.resaas.com/kelvinchan"; //this is the qr code that is generated

//			var qrImage = makeQrCode (qrText);

			//set qrView
			qrView = FindViewById<ImageView> (Resource.Id.qrCode);

			//set userName
			userName = FindViewById<EditText> (Resource.Id.userName);

			//set description
			desc = FindViewById<TextView> (Resource.Id.description);


			//generator button
			Button generate = FindViewById<Button> (Resource.Id.generate_id);
			generate.Click += delegate {
				var qrText = String.Format("www.resaas.com/{0}", userName.Text);
				var qrImage = makeQrCode(qrText);
				qrView.SetImageBitmap(qrImage);

				var descText = String.Format("This qr code is set to: {0} which can be used later to connect the agents. In the core app, it is assumed the user is logged in", qrText);
				desc.Text = descText;
			};

		}

		private Bitmap makeQrCode(string textToConvert){

			var options = new QrCodeEncodingOptions () {
				Height = 300,
				Width = 300,
				Margin = 1
			};

			var writer = new BarcodeWriter();
			writer.Format = BarcodeFormat.QR_CODE;
			writer.Renderer = new BitmapRenderer(){
				Foreground = Color.Black,
				Background = Color.Transparent
			};
			writer.Options = options;

			var bitmap = writer.Write(textToConvert);
			return bitmap;
		}
	}
}

